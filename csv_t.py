import csv

class CsvT:
  _tab = []
  _tabHeaders = []
  
  def _fillVars(self,reader):
    tab = list(reader)
    self._tabHeaders = tab[0]
    self._tab = tab[1:]
  
  def _openFile(self):
    csvFile= open('informations.csv')
    reader = csv.reader(csvFile, delimiter=",", quotechar='"')
    return reader
    
  def getLineNumber(self):
    return len(self._tab)
  
  def getHeader(self):
    return self._tabHeaders
  
  def getLine(self,line):
    return self._tab[line]
  
  def __init__(self):
    print("+ Initialisation de l’objet de type CsvT")
    reader = self._openFile()
    self._fillVars(reader)