# Libraries used
from odf.opendocument import *
from odf import text, presentation, draw

class Template:
  #private attributes
  _doc = ""
  _page = ""
  
  #public methods
  def getTemplate(self):
    return self._page
  
  #privates methods
  def _loadFile(self):
    self._doc = load("./template.odp")
  
  def _defPage(self):  
    self._page = self._doc.getElementsByType(draw.Page)
    print(self._page)
    
      
  def __init__(self):
    print("+ Initialisation de l’objet Template")
    self._loadFile()
    self._defPage()