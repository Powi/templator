from odf import opendocument,text,draw
import copy
import sys

from template import Template
  
class Odtp():
  _doc = ""
  _pageTemplates = []
  
  def saveFile(self):
    self._doc.save("output.odp")
    
  def createPage(self,tabTemp,tabRep):
    pres = self._doc.presentation
    for i in range(len(tabTemp)):
      
      tmp = tabTemp[i]
      trp = tabRep[i]
      print("-> {0} ----> {1}".format(tmp,trp))
      if(i == 0):
        page = copy.deepcopy(self._pageTemplates[int(trp)]) if tmp == "templatePage" else copy.deepcopy(self._pageTemplates[0])
        print("+ Utilisation du template "+str(page))
      elif(tmp[0] == tmp[-1] == "#"):
        print("-> TEXT {0} ----> {1}".format(tmp,trp))
        for p in page.getElementsByType(text.P):
          if(str(p) == tmp):
            print(p.removeChild(p._get_childNodes()[0]))
            print(p.addText(trp))
    page = self._pageTemplates[0]
    pres.addElement(page)
    print("ajout de la page")
    
    
  def createSample(self):
    self._doc.presentation.addElement(self._pageTemplates[0])
  
  def __init__(self,templatePages):
    print("+ Initialisation de l’objet Odtp")   
    self._doc = opendocument.OpenDocumentPresentation()
    self._pageTemplates = templatePages