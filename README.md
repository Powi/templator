# Templator

FR : Écrit par Powi en Python3, GNU GPL v3+, Templator sert à produire une série de pages ODP à partir d’un template et d’un fichier CSV contenant les informations.
Il n’est pas spécialement conçu pour s’adapter à n’importe quel template pour le moment.

EN : Written by Powi in Python3, GNU GPL v3+, Templator create ODP pages from a template and a CSV contaning informations.
It not yet adapting to any usage.

## Needed | Nécéssaire

```
pip3 install odfpy Wand
```
## Comment fonctionne-t-il ? | How does it work ?

### Fichier |template.odp| file

FR : Le fichier Template « template.odp » contient une page de template. Page à adapter en fonction de ses besoins. Cette page servira de modèle pour la sortie.
EN : "template.odp" file contain the template page. To adapt by your needs. This page is the template used to create the output.

### Fichier |informations.csv| file

FR : La première ligne contient les informations à remplacer dans la page Template. Chaque ligne suivante contient les informations qui correspondantes, une ligne par page.
EN : First ligne contain the information to replace, from the Template. Each following line contain the corresponding informations, one line per page.

### Fichier |output.odp| file

FR : Le fichier « output.odp » est le fichier généré par le script.
EN : The « output.odp » file is generated by the script.
