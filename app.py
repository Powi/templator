#!/bin/env python3

# Objects from Templator project
from odtp import Odtp
from csv_t import CsvT
from template import Template
# Libraries used
from wand.image import Image

class App:
  
  def run(self):
    csv = self._csv
    print("+ C’est parti")
    heads = csv.getHeader()
    for line in range(0,csv.getLineNumber()):
      print("+ Création de la page "+str(line))
      self._odtp.createPage(heads,csv.getLine(line))
    self._odtp.saveFile()
  
  def __init__(self):
    print("+ Initialisation de l’Application")
    self._template = Template()
    self._odtp = Odtp(self._template.getTemplate())
    self._csv = CsvT()

if "__main__":
  app = App()
  app.run()